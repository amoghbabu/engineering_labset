import java.util.Scanner;
abstract class Worker
{
    String name;
    float rate;
    Worker(final String n, final float r) {
        name = n;
        rate = r;
    }

    abstract float comPay();
}

class DailyWorker extends Worker {
    private final int hours;

    DailyWorker(final String n, final float r, final int h) {
        super(n, r);
        hours = h;
    }

    public float comPay() {
        final int days = hours / 24;
        return rate * days;
    }
}

class SalariedWorker extends Worker {
    private final int hours;

    SalariedWorker(final String n, final float r, final int h) {
        super(n, r);
        hours = h;
    }
    public float comPay() {
        final int weeks = hours / (24 * 7);
        return rate * weeks;
    }
}

class Work {
    public static void main(final String args[]) {
        String name;
        float rate;
        int time;
        final Scanner ip = new Scanner(System.in);
        System.out.print("Enter Daily Worker name: ");
        name = ip.nextLine();
        System.out.print("Enter rate per day: ");
        rate = ip.nextFloat();
        System.out.print("Enter number of hours: ");
        time = ip.nextInt();
        final DailyWorker dw = new DailyWorker(name, rate, time);
        System.out.println("Salary: " + dw.comPay() + "\n\n");
        ip.nextLine();
        System.out.print("Enter Salaried Worker name: ");
        name = ip.nextLine();
        System.out.print("Enter rate per week: ");
        rate = ip.nextFloat();
        System.out.print("Enter number of hours: ");
        time = ip.nextInt();
        final SalariedWorker sw = new SalariedWorker(name, rate, time);
        System.out.println("Salary: "+sw.comPay());
    }
}