import java.util.Scanner;
public class Matrix
{
    public int row;
    public int column;
    public int[][] a;
    public int[][] b;
    public int[][] data;
    public static void main(String[] args)
    {
        //loop the program if the user prompts "y"
        boolean yes = true;
        do
        {
            Scanner in = new Scanner(System.in);

            //user selects desired matrix operation 
            System.out.print("Please enter your selection: \n");
            System.out.println("Press [1] to perform matrix addition");
            System.out.println("Press [2] to perform matrix multiplication");
            int input = in.nextInt();

            //skip line
            System.out.println();

            System.out.println("Create a matrix: ");
            //takes number of rows for the matrix
            System.out.print("Enter number of rows: ");
            int rowA = in.nextInt();

            //takes number of columns for the matrix
            System.out.print("Enter number of columns: ");
            int columnA = in.nextInt();

            //gathers input for array memory compartments
            Matrix data = new Matrix(rowA, columnA);
            data.matrixDataA();

            //Perform Matrix Addition
            if (input == 1)
            {
                data.addition();
            }
            //Perform Matrix Multiplcation
            if (input == 2)
            {
                data.multiplication();
            }

            //asks the user to re-run the program or not
            System.out.print("Would you like to continue? (y/n): ");
            String check = in.next();

            System.out.println();
            if (check.equalsIgnoreCase("y"))
            {
                yes = true;
            }

            else
            {
                yes = false;
            }

        }
        while(yes == true);
    }
    // Constructs the matrix based on rows and columns
    public Matrix(int ro, int col)
    {
        row = ro;
        column = col;
        a = new int [row][column];
    }

    // Take Input from the User
    public void matrixDataA()
    {
        Scanner in = new Scanner(System.in);
        for (int i = 0; i < row; i++)
        {  
            int u = i + 1;
            System.out.print("Enter values of row " + u + ": ");
            for (int j = 0; j < column; j++)
            {  
                a[i][j] = in.nextInt();
            }
        }
        System.out.println();
    }
    // Printing the 2-dimentional Matrix
    public static void printMatrix(int[][] data, int width)
    {
        for (int i = 0; i < data.length; i++)
        {
            for (int j = 0; j < data[i].length; j++)
            {              
                //separates array values with a space
                System.out.print(data[i][j] + " ");
            }
            System.out.println();
        }
    }
    /**
     * addition, Adds two user-inputted matrices.
     */
    public void addition()
    {
        Scanner in = new Scanner(System.in);
        //makes second matrix
        System.out.println("Second matrix: ");

        int[][] b = new int[row][column];
        for (int i = 0; i < row; i++)
        {  
            int u = i + 1;
            System.out.print("Enter values of row " + u + ": ");
            for (int j = 0; j < column; j++)
            {  
                b[i][j] = in.nextInt();
            }
        }
        System.out.println();

        //final result matrix "add"
        int[][] add = new int[row][column];
        for(int i = 0; i < row; i++)
        {
            for (int j = 0; j < column; j++)
            {
                add[i][j] = a[i][j] + b[i][j];
            }
        }       
        //prints the final result
        System.out.println("Resultant Matrix of Addition:");
        printMatrix(add, column);
    }
    /**
     * multiplication, Multiplies two user-inputted matrices, upholding the condition that the number of columns of the first matrix
     * is equal to the number of rows of the second matrix.
     */
    public void multiplication()
    {
        Scanner in = new Scanner(System.in);
        //creates second matrix
        System.out.println("Second matrix: ");

        System.out.print("Enter number of rows: ");
        int rowB = in.nextInt();
        System.out.print("Enter number of columns: ");
        int columnB = in.nextInt();

        //if dimensions do not fit the Matrix Multiplication condition, return "Dimensions mismatch..."
        //else, perform the operation
        if (column != rowB)
        {
            System.out.println("Dimensions mismatch! Unable to complete the computation!!! DUMMY");
        }
        else
        {
            int[][] b = new int[rowB][columnB];
            for (int i = 0; i < rowB; i++)
            {  
                int u = i + 1;
                System.out.print("Enter values of row " + u + ": ");
                for (int j = 0; j < columnB; j++)
                {  
                    b[i][j] = in.nextInt();
                }
            }
            System.out.println();

            //final result matrix "mult"
            int[][] mult = new int[row][columnB];

            for(int i = 0; i < row; i++)
            {
                for (int j = 0; j < columnB; j++)
                {
                    int sum = 0;
                    for(int k = 0; k < rowB; k++)
                    {
                        sum = sum + ( a[i][k] * b[k][j] );
                    }
                    mult[i][j] = sum;
                }
            }
            //prints the final matrix
            System.out.println("Resultant Matrix of Multiplication:");
            printMatrix(mult, columnB);
        }
    }
}